db.users.insertOne({
    
        "username": "dahyunKim",
        "password": "once1234"
    
    
})

db.users.insertOne({
    
        "username": "teejaecalinao",
        "password": "tjc1234"
    
})

db.users.insertMany([

    {
        "username": "pablo123",
        "password": "123paul"
    },
    {
        "username": "pedro99",
        "password": "iampeter99"
    }

])

//commands for creating documents:
/*

        db.collection.insertOne({
                

                "filed1":"value",
                "field2":"value"

        })


        db.collection.instertMane([
        
                {
                        "field1":"valueA",
                        "field2":"valueB"
                },
                {
                        "field1":"valueA",
                        "field2":"valueB"
                },

        ])

*/

//command for collecting all documents
// db.collection.find()

// for specific document:
       // db.collection.find({"field":"value"})

/*
db.cars.insertMany([

    {
        name: "Vios",
        brand: "Toyota",
        type: "sedan",
        price: 1500000
    },
    {
        name: "Tamaraw FX",
        brand: "Toyota",
        type: "auv",
        price: 750000
    },
    {
        name: "City",
        brand: "Honda",
        type: "sedan",
        price: 1600000     
    }

])


*/


/*
//Mini-Activity:
// 1. Retrieve/find all car documents.
// 2. Retrieve/find all car documents which type is "sedan".
// Screenshot your output and send it on the hangouts

db.cars.find()

db.cars.find({type:"sedan"})
*/

/*
command for updating:
//updateOne() - allows us to update the first item that matches our criteria
//db.users.updateOne({username:"pedro99"},{$set:{username: "peter1999"}})
//db.users.updateOne({},{$set:{username:"dahyunieTwice"}})
db.users.updateOne({username:"pablo123"},{$set:{isAdmin: true}})
*/

//update() - allows us to udpate all documents that matches the criteria
//db.users.updateMany({},{$set:{isAdmin:true}})
//db.cars.updateMany({type:"sedan"},{$set:{price:1000000}})


//deleteOne() - delete the first item that matches the criteria
//db.users.deleteOne({})
//db.cars.deleteOne({brand:"Toyota"})

//deleteMany() delete all items that matches the criteria
// db.users.deleteMany({isAdmin:true})
//db.cars.deleteMany({})